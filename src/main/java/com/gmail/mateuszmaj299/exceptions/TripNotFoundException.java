package com.gmail.mateuszmaj299.exceptions;

public class TripNotFoundException extends RuntimeException{
    public TripNotFoundException(String message) {
        super(message);
    }

}
