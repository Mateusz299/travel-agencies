package com.gmail.mateuszmaj299.exceptions;

public class ReservationValidationException extends RuntimeException{
    public ReservationValidationException(String message) {
        super(message);
    }
}
