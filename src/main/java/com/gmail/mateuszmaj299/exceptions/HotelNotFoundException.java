package com.gmail.mateuszmaj299.exceptions;

public class HotelNotFoundException extends RuntimeException{
    public HotelNotFoundException(String message) {
        super(message);
    }

}
