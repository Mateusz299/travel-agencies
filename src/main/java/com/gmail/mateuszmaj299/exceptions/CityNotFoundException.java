package com.gmail.mateuszmaj299.exceptions;

public class CityNotFoundException extends RuntimeException{
        public CityNotFoundException(String message) {
        super(message);
    }
}
