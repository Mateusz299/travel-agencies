package com.gmail.mateuszmaj299.exceptions;

public class AirportNotFoundException extends RuntimeException{
    public AirportNotFoundException(String message) {
        super(message);
    }

}
