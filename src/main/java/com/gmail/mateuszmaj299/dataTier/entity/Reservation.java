package com.gmail.mateuszmaj299.dataTier.entity;


import com.sun.istack.NotNull;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "RESERVATION")
public class Reservation {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Integer id;
    @NotNull
    @Column(name = "PRICE")
    private BigDecimal price;
    @NotBlank
    @Column(name = "HOTEL_NAME")
    private String hotelName;

    @ManyToOne
    @JoinColumn(name = "hotel_id")
    private Hotel hotel;
    @NotBlank
    @Column(name = "CITY_NAME")
    private String cityName;

    @Column(name = "USER_ID")
    private Integer userId;

    @Column(name = "TRIP_ID")
    private Integer tripId;
    @NotNull
    @Column(name = "RATING")
    private Float rating;
    @NotBlank
    @Column(name = "DESCRIPTION")
    private String description;
    @CreatedDate
    @Column(updatable = false)
    private Date createdDate;

    @LastModifiedDate
    private Date lastModifiedDate;

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(Date lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String getHotelName() {
        return hotelName;
    }

    public void setHotelName(String hotelName) {
        this.hotelName = hotelName;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getTripId() {
        return tripId;
    }

    public void setTripId(Integer tripId) {
        this.tripId = tripId;
    }

    public Float getRating() {
        return rating;
    }

    public void setRating(Float rating) {
        this.rating = rating;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Hotel getHotel() {
        return hotel;
    }

    public Reservation(String hotelName, Integer userId, Float rating) {
        this.hotelName = hotelName;
        this.userId = userId;
        this.rating = rating;
    }

    public Reservation() {
    }

    public void setHotel(Hotel hotel) {
        this.hotel = hotel;
    }
}
