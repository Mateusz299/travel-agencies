package com.gmail.mateuszmaj299.dataTier.entity;

import com.sun.istack.NotNull;

import javax.validation.constraints.Size;

import javax.persistence.*;

@Entity
@Table(name = "AIRPORT")
public class Airport {
    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer ID;
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "CITY_ID", referencedColumnName = "ID")
    private City city;
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "NAME")
    private String name;
    private String address;

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }


    public Integer getID() {
        return ID;
    }

    public void setID(Integer ID) {
        this.ID = ID;
    }

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public Airport(String name, City city) {
        this.name = name;
        this.city = city;
    }
    public Airport() {
    }

    public Airport(Integer ID, City city, String name, String address) {
        this.ID = ID;
        this.city = city;
        this.name = name;
        this.address = address;
    }
}
