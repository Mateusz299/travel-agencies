package com.gmail.mateuszmaj299.presentationTier.controller;


import com.gmail.mateuszmaj299.logicTier.DTO.HotelDTO;
import com.gmail.mateuszmaj299.logicTier.service.HotelService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;

import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;


@RestController
@RequestMapping("/hotels")
public class HotelController {


    private final HotelService hotelService;

    @Autowired
    public HotelController(HotelService hotelService) {
        this.hotelService = hotelService;
    }
    @CrossOrigin(origins = "http://localhost:4200/", maxAge = 3600)
        @GetMapping("/list")
    public ResponseEntity<List<HotelDTO>> listHotels(){
        List<HotelDTO> hotels = hotelService.findAllHotels();
        return ResponseEntity.ok(hotels);
    }

    @Secured("ROLE_ADMIN")
    @GetMapping("/{name}")
    public HotelDTO getHotel(@PathVariable String name) throws RuntimeException {
        return hotelService.getHotel(name);
    }

    @GetMapping("/filterByCity")
    public List<HotelDTO> getHotelsByCityName(@RequestParam String cityName) throws RuntimeException {
        return hotelService.getHotelsByCityName(cityName);
    }

    @Secured("ROLE_ADMIN")
    @DeleteMapping("/{hotelName}")
    public ResponseEntity<HotelDTO> deleteHotel(@PathVariable String hotelName) throws RuntimeException {
        HotelDTO hotelDto = hotelService.deleteHotel(hotelName);
        return new ResponseEntity<>(hotelDto, HttpStatus.OK);
    }

    @Secured("ROLE_ADMIN")
    @PutMapping("/{hotelName}")
    public ResponseEntity<HotelDTO> updateHotel(@PathVariable String hotelName, @Valid @RequestBody HotelDTO hotelDTO) throws RuntimeException {
        hotelService.updateHotel(hotelName, hotelDTO);
        return new ResponseEntity<>(hotelDTO, HttpStatus.OK);
    }

    @Secured("ROLE_ADMIN")
    @PostMapping("/addHotel")
    ResponseEntity<HotelDTO> addHotel(@Valid @RequestBody HotelDTO hotelDTO) {
        hotelService.addHotel(hotelDTO);
        return new ResponseEntity<>(hotelDTO, HttpStatus.OK);
    }
}
