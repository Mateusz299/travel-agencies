package com.gmail.mateuszmaj299.presentationTier.controller;

import com.gmail.mateuszmaj299.dataTier.entity.Reservation;
import com.gmail.mateuszmaj299.logicTier.DTO.ReservationDemands;
import com.gmail.mateuszmaj299.logicTier.mapper.ReservationMapper;
import com.gmail.mateuszmaj299.logicTier.service.ReservationService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/reservations")
public class ReservationController {
    private final ReservationService reservationService;
    private final ReservationMapper reservationMapper;

    public ReservationController(ReservationService reservationService, ReservationMapper reservationMapper) {
        this.reservationService = reservationService;
        this.reservationMapper = reservationMapper;

    }

    @GetMapping("/reservation")
    public List<Reservation> getAllReservations() {
        return reservationService.findAllReservations();
    }

    @GetMapping("/{id}")
    public ResponseEntity<Reservation> getReservationById(@PathVariable int id) {
        return reservationService.findReservationById(id)
                .map(ResponseEntity::ok)
                .orElse(ResponseEntity.notFound().build());
    }

    @PostMapping
    public ResponseEntity<?> addReservation(@Valid @RequestBody ReservationDemands reservationDemands) {
        try {
            Reservation reservation = reservationService.addReservation(reservationMapper.reservationDemandsToReservation(reservationDemands));
            return ResponseEntity.status(HttpStatus.CREATED).body(reservation);
        } catch (Exception e) {
            return ResponseEntity.badRequest().body("Invalid reservation data: " + e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteReservation(@PathVariable int id) {
        boolean isDeleted = reservationService.deleteReservation(id);
        if (isDeleted) {
            return ResponseEntity.ok().build();
        }
        return ResponseEntity.notFound().build();
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> updateReservation(@PathVariable int id, @Valid @RequestBody ReservationDemands reservationDemands) {
        try {
            Optional<Reservation> updatedReservation = reservationService.updateReservation(id, reservationMapper.reservationDemandsToReservation(reservationDemands));
            return updatedReservation.map(ResponseEntity::ok)
                    .orElseGet(() -> ResponseEntity.notFound().build());
        } catch (Exception e) {
            return ResponseEntity.badRequest().body("Invalid reservation data: " + e.getMessage());
        }
    }
}
