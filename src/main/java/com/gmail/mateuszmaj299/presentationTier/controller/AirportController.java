package com.gmail.mateuszmaj299.presentationTier.controller;

import com.gmail.mateuszmaj299.dataTier.entity.Airport;
import com.gmail.mateuszmaj299.logicTier.DTO.AirportDTO;
import com.gmail.mateuszmaj299.logicTier.service.AirportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/airports")

public class AirportController {
    private final AirportService airportService;

    @Autowired
    public AirportController(AirportService airportService) {
        this.airportService = airportService;

    }

    @GetMapping("/list")
    public ModelAndView getAllAirports() {
        ModelAndView modelAndView = new ModelAndView("airports");
        try {
            List<AirportDTO> airports = airportService.getAllAirports();
            if (!airports.isEmpty()) {
                airports.sort(Comparator.comparing(AirportDTO::getName));
            }
            modelAndView.addObject("airports", airports);
        } catch (Exception e) {
            modelAndView.addObject("airports", new ArrayList<Airport>());
            modelAndView.addObject("error", "An error occurred while downloading the list of airports.");
        }
        return modelAndView;
    }

    @Secured("ROLE_ADMIN")
    @DeleteMapping("/{airportName}")
    public ResponseEntity<AirportDTO> deleteAirport(@PathVariable String airportName) throws RuntimeException {
        AirportDTO airportDTO = airportService.deleteAirport(airportName);
        return new ResponseEntity<>(airportDTO, HttpStatus.OK);
    }

    @Secured("ROLE_ADMIN")
    @PostMapping("/addAirport")
    public ResponseEntity<AirportDTO> addAirport(@Valid @RequestBody AirportDTO airportDTO) {
        airportService.addAirport(airportDTO);
        return new ResponseEntity<>(airportDTO, HttpStatus.OK);
    }

    @Secured("ROLE_USER")
    @GetMapping("/showAirportByCity")
    List<AirportDTO> getAirportsByCityName(@RequestParam String cityName) {
        try {
            return airportService.getAirportsByCity(cityName);
        } catch (Exception e) {

            throw new RuntimeException("Error when filtering airports by city name.", e);
        }
    }

    @Secured("ROLE_ADMIN")
    @PutMapping("/{airportName}")
    ResponseEntity<AirportDTO> updateHotel(@PathVariable String airportName, @Valid @RequestBody AirportDTO airportDTO) throws RuntimeException {
        airportService.updateAirport(airportName, airportDTO);
        return new ResponseEntity<>(airportDTO, HttpStatus.OK);
    }

}