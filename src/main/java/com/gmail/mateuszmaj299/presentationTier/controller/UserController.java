package com.gmail.mateuszmaj299.presentationTier.controller;

import com.gmail.mateuszmaj299.dataTier.entity.Customer;
import com.gmail.mateuszmaj299.logicTier.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.util.List;
    @RestController
    @RequestMapping("/api/users")
    public class UserController {

        private final AccountService accountService;

        @Autowired
        public UserController(AccountService accountService) {
            this.accountService = accountService;
        }

        @GetMapping
        public List<Customer> getAllUsers() {
            return accountService.findAllUsers();
        }

        @GetMapping("/{id}")
        public ResponseEntity<Customer> getUserById(@PathVariable int id) {
            return accountService.findUserById(id)
                    .map(ResponseEntity::ok)
                    .orElse(ResponseEntity.notFound().build());
        }

        @PutMapping("/{id}")
        public ResponseEntity<Customer> updateUser(@PathVariable int id, @RequestBody Customer customerDetails) {
            return accountService.updateUser(id, customerDetails)
                    .map(ResponseEntity::ok)
                    .orElse(ResponseEntity.notFound().build());
        }
        @Secured("ROLE_ADMIN")
        @DeleteMapping("/{id}")
        public ResponseEntity<?> deleteUser(@PathVariable int id) {
            if (accountService.deleteUser(id)) {
                return ResponseEntity.ok().build();
            }
            return ResponseEntity.notFound().build();
        }
    }
