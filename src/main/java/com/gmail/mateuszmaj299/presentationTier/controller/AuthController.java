package com.gmail.mateuszmaj299.presentationTier.controller;


import com.gmail.mateuszmaj299.logicTier.DTO.AccountRegistrationDTO;
import com.gmail.mateuszmaj299.logicTier.security.EmailValidator;
import com.gmail.mateuszmaj299.logicTier.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


@RestController
public class AuthController {

    private final AccountService accountService;
    private final AuthenticationManager authenticationManager;
    private static final Pattern PASSWORD_PATTERN = Pattern.compile("^(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])(?=.*[!@#$%^&*(),.?\":{}|<>])\\S+$");

    @Autowired
    public AuthController(AccountService accountService, AuthenticationManager authenticationManager) {
        this.accountService = accountService;
        this.authenticationManager = authenticationManager;
    }
    private boolean isPasswordValid(String password) {
        Matcher matcher = PASSWORD_PATTERN.matcher(password);
        return matcher.matches();
    }

    @PostMapping("/login")
    public ResponseEntity<?> loginUser(@ModelAttribute AccountRegistrationDTO accountRegistrationDTO) {
        try {
            Authentication authentication = authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(
                            accountRegistrationDTO.getEmailAddress(),
                            accountRegistrationDTO.getPassword()
                    )
            );

            SecurityContextHolder.getContext().setAuthentication(authentication);
            return ResponseEntity.ok().body("User authenticated successfully");
        } catch (AuthenticationException e) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Authentication failed: " + e.getMessage());
        }
    }

    @PostMapping("/register")
    public ResponseEntity<?> registerUser(@RequestBody AccountRegistrationDTO accountRegistrationDTO) {
        if (!EmailValidator.isValidEmail(accountRegistrationDTO.getEmailAddress())) {
            return ResponseEntity.badRequest().body(Map.of("message", "Invalid email address format"));
        }

        if (accountService.findUserByEmail(accountRegistrationDTO.getEmailAddress()).isPresent()) {
            return ResponseEntity.badRequest().body(Map.of("message", "Email already in use"));
        }

        if (!isPasswordValid(accountRegistrationDTO.getPassword())) {
            return ResponseEntity.badRequest().body("Invalid password format");
        }

        accountService.registerUser(accountRegistrationDTO);
        return ResponseEntity.ok(Map.of("status", "success", "message", "User registered successfully"));
    }
}
