package com.gmail.mateuszmaj299;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import springfox.documentation.swagger2.annotations.EnableSwagger2;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;

import java.util.EventListener;

@SpringBootApplication
@EnableSwagger2
@ConfigurationPropertiesScan
public class TravelAgenciesApplication {

	public static void main(String[] args) {
		SpringApplication.run(TravelAgenciesApplication.class, args);
	}
}
