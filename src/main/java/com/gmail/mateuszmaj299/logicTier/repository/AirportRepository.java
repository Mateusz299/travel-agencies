package com.gmail.mateuszmaj299.logicTier.repository;

import com.gmail.mateuszmaj299.dataTier.entity.Airport;

import com.gmail.mateuszmaj299.dataTier.entity.City;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

public interface AirportRepository extends JpaRepository<Airport, Integer> {
    @Query("SELECT a FROM Airport a JOIN a.city c WHERE lower(c.name) = lower(:cityName)")
    List<Airport> findByCityNameIgnoreCase(@Param("cityName") String cityName);

    @Query("SELECT a FROM Airport a WHERE lower(a.name) = lower(:name)")
    Optional<Airport> findByName(@Param("name") String name);
}
