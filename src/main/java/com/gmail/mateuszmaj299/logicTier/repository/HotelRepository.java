package com.gmail.mateuszmaj299.logicTier.repository;

import com.gmail.mateuszmaj299.dataTier.entity.Hotel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface HotelRepository extends JpaRepository <Hotel, Integer> {

    @Query("SELECT h FROM Hotel h WHERE lower(h.name) = lower(:name)")
    Optional<Hotel> findByName(String name);

    @Query("SELECT h FROM Hotel h JOIN h.city c WHERE lower(c.name) = lower(:cityName)")
    List<Hotel> findByCityName(String cityName);
}
