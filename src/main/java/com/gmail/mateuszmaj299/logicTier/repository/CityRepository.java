package com.gmail.mateuszmaj299.logicTier.repository;

import com.gmail.mateuszmaj299.dataTier.entity.City;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

public interface CityRepository extends JpaRepository<City, Integer> {
    @Query("SELECT c FROM City c WHERE lower(c.name) = lower(:name)")
    Optional<City> findCityByName (String name);
}
