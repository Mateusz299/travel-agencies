package com.gmail.mateuszmaj299.logicTier.repository;

import com.gmail.mateuszmaj299.dataTier.entity.Reservation;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface ReservationRepository extends JpaRepository <Reservation, Integer> {

    List<Reservation> findByUserId(Integer userId);

    List<Reservation> findByHotelNameContaining(String hotelName);

    List<Reservation> findByRatingBetween(Float minRating, Float maxRating);

}
