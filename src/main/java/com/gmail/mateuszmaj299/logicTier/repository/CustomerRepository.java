package com.gmail.mateuszmaj299.logicTier.repository;

import com.gmail.mateuszmaj299.dataTier.entity.Customer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

public interface CustomerRepository extends JpaRepository<Customer, Integer> {

    @Query("SELECT c FROM Customer c WHERE lower(c.emailAddress) = lower(:email)")
    Optional<Customer> findByEmailAddress(String email);
    @Query("SELECT c FROM Customer c WHERE c.id = :id")
    Optional<Customer> findById(Integer id);
}
