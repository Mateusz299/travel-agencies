//package com.gmail.mateuszmaj299.logicTier.repository;
//
//import com.gmail.mateuszmaj299.dataTier.entity.Customer;
//import org.springframework.data.jpa.repository.JpaRepository;
//
//import java.util.Optional;
//
//public interface AccountRepository extends JpaRepository<Customer, Integer> {
//    Optional<Customer> findByEmailAddress(String email);
//
//}