package com.gmail.mateuszmaj299.logicTier.mapper;

import com.gmail.mateuszmaj299.dataTier.entity.Airport;
import com.gmail.mateuszmaj299.logicTier.DTO.AirportDTO;
import com.gmail.mateuszmaj299.logicTier.repository.AirportRepository;
import org.springframework.stereotype.Component;

@Component
public class AirportMapper {

    private final AirportRepository airportRepository;

    public AirportMapper(AirportRepository airportRepository) {
        this.airportRepository = airportRepository;

    }

    public Airport AirportDTOToAirport(AirportDTO airportDto){
        Airport mappedAirport = new Airport();
        mappedAirport.setName(airportDto.getName());
        return mappedAirport;
    }
    public AirportDTO AirportToAirportDTO(Airport airport){
        AirportDTO airportDTO = new AirportDTO();
        airportDTO.setName(airport.getName());
        airportDTO.setCityName(String.valueOf(airport.getCity()));
        return airportDTO;
    }
}
