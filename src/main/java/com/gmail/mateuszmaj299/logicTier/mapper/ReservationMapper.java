package com.gmail.mateuszmaj299.logicTier.mapper;

import com.gmail.mateuszmaj299.dataTier.entity.Reservation;
import com.gmail.mateuszmaj299.logicTier.DTO.ReservationDTO;
import com.gmail.mateuszmaj299.logicTier.DTO.ReservationDemands;
import com.gmail.mateuszmaj299.logicTier.repository.HotelRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ReservationMapper {
    private final HotelRepository hotelRepository;

    @Autowired
    public ReservationMapper(HotelRepository hotelRepository) {
        this.hotelRepository = hotelRepository;
    }

    public Reservation reservationDemandsToReservation(ReservationDemands reservationDemands) {
        Reservation mappedReservation = new Reservation();
        mappedReservation.setHotelName(reservationDemands.getHotelName());
        mappedReservation.setCityName(reservationDemands.getCityName());
        mappedReservation.setPrice(reservationDemands.getPrice());
        mappedReservation.setRating(reservationDemands.getRating());
        mappedReservation.setDescription(reservationDemands.getDescription());
        return mappedReservation;
    }

    public ReservationDTO reservationToReservationDTO(Reservation reservation) {
        ReservationDTO reservationDTO = new ReservationDTO();
        reservationDTO.setHotelName(reservation.getHotelName());
        reservationDTO.setCityName(reservation.getCityName());
        reservationDTO.setPrice(reservation.getPrice());
        reservationDTO.setRating(reservation.getRating());
        reservationDTO.setDescription(reservation.getDescription());
        return reservationDTO;
    }
}
