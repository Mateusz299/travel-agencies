package com.gmail.mateuszmaj299.logicTier.mapper;

import com.gmail.mateuszmaj299.dataTier.entity.Hotel;
import com.gmail.mateuszmaj299.exceptions.CityNotFoundException;
import com.gmail.mateuszmaj299.logicTier.DTO.HotelDTO;
import com.gmail.mateuszmaj299.logicTier.repository.CityRepository;
import com.gmail.mateuszmaj299.logicTier.repository.HotelRepository;
import org.springframework.stereotype.Component;

@Component
public class HotelMapper {

    private final HotelRepository hotelRepository;
    private final CityRepository cityRepository;

    public HotelMapper(HotelRepository hotelRepository, CityRepository cityRepository) {
        this.hotelRepository = hotelRepository;
        this.cityRepository = cityRepository;
    }



    public Hotel hotelDTOtoHotel(HotelDTO hotelDTO){
        Hotel mappedHotel = new Hotel();
        mappedHotel.setName(hotelDTO.getName());
        mappedHotel.setDescription(hotelDTO.getDescription());
        mappedHotel.setRating(hotelDTO.getRating());
        mappedHotel.setAddress(hotelDTO.getAddress());
        mappedHotel.setCity(cityRepository.findCityByName(hotelDTO.getCityName()).orElseThrow(() -> new CityNotFoundException("That city do not exist")));
        mappedHotel.setImage(hotelDTO.getImage());
        return mappedHotel;
    }


    public HotelDTO hotelToHotelDto(Hotel hotel){
        HotelDTO hotelDTO = new HotelDTO();
        hotelDTO.setName(hotel.getName());
        hotelDTO.setDescription(hotel.getDescription());
        hotelDTO.setRating(hotel.getRating());
        hotelDTO.setAddress(hotel.getAddress());
        hotelDTO.setCityName(hotel.getCity().getName());
        hotelDTO.setImage(hotel.getImage());
        return hotelDTO;

    }
}