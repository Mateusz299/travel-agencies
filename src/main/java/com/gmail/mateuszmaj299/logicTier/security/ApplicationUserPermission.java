package com.gmail.mateuszmaj299.logicTier.security;


public enum ApplicationUserPermission {
    APP_READ("app:read"),
    APP_WRITE("app:write");

    private final String permission;

    ApplicationUserPermission(String permission) {
        this.permission = permission;
    }
    public String getPermission() {
        return permission;
    }

}
