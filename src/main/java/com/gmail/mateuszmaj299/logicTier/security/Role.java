package com.gmail.mateuszmaj299.logicTier.security;


import com.google.common.collect.Sets;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import static com.gmail.mateuszmaj299.logicTier.security.ApplicationUserPermission.*;

import java.util.Set;
import java.util.stream.Collectors;


public enum Role {
    ROLE_ADMIN(Sets.newHashSet(APP_READ, APP_WRITE)),
    ROLE_CUSTOMER(Sets.newHashSet()),
    ;

    private final Set<ApplicationUserPermission> permissions;

    Role(Set<ApplicationUserPermission> permissions) {
        this.permissions = permissions;
    }

    public Set<ApplicationUserPermission> getPermissions() {
        return permissions;
    }
    public Set<SimpleGrantedAuthority> getGrantedAuthorities() {
        Set<SimpleGrantedAuthority> permissions = getPermissions().stream().map(permission -> new SimpleGrantedAuthority(permission.getPermission())).collect(Collectors.toSet());
        permissions.add(new SimpleGrantedAuthority("ROLE_" + this.name()));
        return permissions;
    }
}
