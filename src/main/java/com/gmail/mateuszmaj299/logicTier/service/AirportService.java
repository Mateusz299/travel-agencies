package com.gmail.mateuszmaj299.logicTier.service;

import com.gmail.mateuszmaj299.dataTier.entity.Airport;
import com.gmail.mateuszmaj299.dataTier.entity.City;
import com.gmail.mateuszmaj299.exceptions.AirportNotFoundException;
import com.gmail.mateuszmaj299.exceptions.CityNotFoundException;
import com.gmail.mateuszmaj299.logicTier.DTO.AirportDTO;
import com.gmail.mateuszmaj299.logicTier.mapper.AirportMapper;
import com.gmail.mateuszmaj299.logicTier.repository.AirportRepository;
import com.gmail.mateuszmaj299.logicTier.repository.CityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class AirportService {

    private final AirportRepository airportRepository;
    private final AirportMapper airportMapper;
    private final CityRepository cityRepository;

    @Autowired
    public AirportService(AirportRepository airportRepository, AirportMapper airportMapper, CityRepository cityRepository) {
        this.cityRepository = cityRepository;
        this.airportRepository = airportRepository;
        this.airportMapper = airportMapper;
    }

    public List<AirportDTO> getAllAirports(){
        return airportRepository.findAll().stream().map(airportMapper::AirportToAirportDTO).toList();
    }

    public List<AirportDTO> getAirportsByCity(String cityName) {
    if (cityName == null || cityName.isEmpty()) {
        throw new IllegalArgumentException("City name cannot be empty");
    }

    City city = cityRepository.findCityByName(cityName)
            .orElseThrow(() -> new CityNotFoundException("That city does not exist"));

    return airportRepository.findByCityNameIgnoreCase(cityName)
            .stream()
            .map(airportMapper::AirportToAirportDTO)
            .collect(Collectors.toList());
}

    public void addAirport(AirportDTO airportDTO){
        airportRepository.save(airportMapper.AirportDTOToAirport(airportDTO));
    }

    public void updateAirport(String airportName, AirportDTO airportDTO){
        Airport airport = airportRepository.findByName(airportName)
                .orElseThrow(() -> new AirportNotFoundException("That airport do not exist"));
        airport.setName(airportDTO.getName());
        airport.setAddress(airportDTO.getAddress());
        airportRepository.save(airport);
    }

    public AirportDTO deleteAirport(String name) {
        Airport airport = airportRepository.findByName(name)
                .orElseThrow(() -> new AirportNotFoundException("No such airport exists"));
        airportRepository.delete(airport);
        return airportMapper.AirportToAirportDTO(airport);
    }
}