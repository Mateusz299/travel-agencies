//package com.gmail.mateuszmaj299.logicTier.service;
//
//import com.gmail.mateuszmaj299.logicTier.repository.PaymentRepository;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//
//@Service
//public class PaymentService {
//
//    private final PaymentRepository paymentRepository;
//    private final CustomerWalletRepository customerWalletRepository;
//
//    @Autowired
//    public PaymentService(PaymentRepository paymentRepository, CustomerWalletRepository userWalletRepository) {
//        this.paymentRepository = paymentRepository;
//        this.customerWalletRepository = userWalletRepository;
//    }

//    public Optional<Payment> makePayment(int userId, int tripId, BigDecimal amount) {
//        Optional<UserWallet> userWalletOpt = userWalletRepository.findByUserId(userId);
//
//        if (userWalletOpt.isPresent()) {
//            UserWallet wallet = userWalletOpt.get();
//            BigDecimal balance = wallet.getBalance();
//
//            if (balance.compareTo(amount) >= 0) {
//
//                wallet.setBalance(balance.subtract(amount));
//                userWalletRepository.save(wallet);
//
//                Payment payment = new Payment();
//                payment.setUserId(userId);
//                payment.setTripId(tripId);
//                payment.setAmount(amount);
//                payment.setStatus("Paid");
//                return Optional.of(paymentRepository.save(payment));
//            } else {
//                throw new RuntimeException("Insufficient funds in wallet");
//            }
//        } else {
//            throw new RuntimeException("User wallet not found");
//        }
//    }

    // Add $$, Checkout account ect.
    // ...

//}