package com.gmail.mateuszmaj299.logicTier.service;

import com.gmail.mateuszmaj299.dataTier.entity.Customer;
import com.gmail.mateuszmaj299.logicTier.DTO.AccountRegistrationDTO;
import com.gmail.mateuszmaj299.logicTier.repository.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;


import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
public class AccountService implements UserDetailsService {

    private final CustomerRepository customerRepository;
    private final PasswordEncoder passwordEncoder;

    @Autowired
    public AccountService(CustomerRepository customerRepository, PasswordEncoder passwordEncoder) {
        this.customerRepository = customerRepository;
        this.passwordEncoder = passwordEncoder;
    }

    public Customer registerUser(AccountRegistrationDTO accountRegistrationDTO) {
        Customer customer = new Customer();
        customer.setEmailAddress(accountRegistrationDTO.getEmailAddress());
        customer.setPassword(passwordEncoder.encode(accountRegistrationDTO.getPassword()));
        return customerRepository.save(customer);
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Customer customer = customerRepository.findByEmailAddress(username)
                .orElseThrow(() -> new UsernameNotFoundException("User not found with email: " + username));

        Set<SimpleGrantedAuthority> grantedAuthorities = new HashSet<>();
        grantedAuthorities.add(new SimpleGrantedAuthority(customer.getRole()));

        return new User(customer.getEmailAddress(), customer.getPassword(), grantedAuthorities);
    }

    public Optional<Customer> findUserById(int id) {
        return customerRepository.findById(id);
    }

    public Optional<Customer> findUserByEmail(String email) {
        return customerRepository.findByEmailAddress(email);
    }

    public List<Customer> findAllUsers() {
        return customerRepository.findAll();
    }

    public Optional<Customer> updateUser(int id, Customer userDetails) {
        return customerRepository.findById(id)
                .map(user -> {
                    user.setFirstName(userDetails.getFirstName());
                    user.setLastName(userDetails.getLastName());
                    user.setEmailAddress(userDetails.getEmailAddress());
                    return customerRepository.save(user);
                });
    }

    public boolean deleteUser(int id) {
        return customerRepository.findById(id)
                .map(user -> {
                    customerRepository.delete(user);
                    return true;
                }).orElse(false);
    }

}