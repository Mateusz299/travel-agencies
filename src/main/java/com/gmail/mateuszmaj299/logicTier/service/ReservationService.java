package com.gmail.mateuszmaj299.logicTier.service;

import com.gmail.mateuszmaj299.dataTier.entity.Reservation;
import com.gmail.mateuszmaj299.exceptions.ReservationNotFoundException;
import com.gmail.mateuszmaj299.exceptions.ReservationValidationException;
import com.gmail.mateuszmaj299.logicTier.repository.ReservationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;


@Service
public class ReservationService {

    private final ReservationRepository reservationRepository;

    @Autowired
    public ReservationService(ReservationRepository reservationRepository) {
        this.reservationRepository = reservationRepository;
    }

    public List<Reservation> findAllReservations() {
        return reservationRepository.findAll();
    }

    public Optional<Reservation> findReservationById(Integer id) {
        return reservationRepository.findById(id);
    }

    public Reservation addReservation(Reservation reservation) {
        return reservationRepository.save(reservation);
    }

    public boolean deleteReservation(Integer id) {
        if (!reservationRepository.existsById(id)) {
            throw new ReservationNotFoundException("Reservation with ID " + id + " not found");
        }
        reservationRepository.deleteById(id);
        return true;
    }

    public Optional<Reservation> updateReservation(Integer id, Reservation reservation) {
        return reservationRepository.findById(id).map(existingReservation -> {
            validateReservation(reservation);
            updateExistingReservation(existingReservation, reservation);
            return reservationRepository.save(existingReservation);
        }).map(Optional::of).orElseThrow(() -> new ReservationValidationException("Reservation with ID " + id + " not found"));
    }

    private void validateReservation(Reservation reservation) {
        if (reservation.getPrice() != null && reservation.getPrice().compareTo(BigDecimal.ZERO) < 0) {
            throw new ReservationValidationException("Price cannot be negative");
        }
        if (reservation.getHotelName() == null || reservation.getHotelName().trim().isEmpty()) {
            throw new ReservationValidationException("Hotel name cannot be empty");
        }
        if (reservation.getRating() != null && (reservation.getRating() < 1.0 || reservation.getRating() > 5.0)) {
            throw new ReservationValidationException("Rating must be between 1.0 and 5.0");
        }
    }

    private void updateExistingReservation(Reservation existingReservation, Reservation newReservation) {
        existingReservation.setHotelName(newReservation.getHotelName());
        existingReservation.setCityName(newReservation.getCityName());
        existingReservation.setPrice(newReservation.getPrice());
        existingReservation.setRating(newReservation.getRating());
        existingReservation.setDescription(newReservation.getDescription());
    }
}