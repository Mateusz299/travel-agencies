package com.gmail.mateuszmaj299.logicTier.service;

import com.gmail.mateuszmaj299.dataTier.entity.Hotel;
import com.gmail.mateuszmaj299.exceptions.CityNotFoundException;
import com.gmail.mateuszmaj299.exceptions.HotelNotFoundException;
import com.gmail.mateuszmaj299.logicTier.DTO.HotelDTO;
import com.gmail.mateuszmaj299.logicTier.mapper.HotelMapper;
import com.gmail.mateuszmaj299.logicTier.repository.CityRepository;
import com.gmail.mateuszmaj299.logicTier.repository.HotelRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class HotelService {

    private final HotelRepository hotelRepository;
    private final HotelMapper hotelMapper;
    private final CityRepository cityRepository;

    @Autowired
    public HotelService(HotelRepository hotelRepository, HotelMapper hotelMapper, CityRepository cityRepository) {
        this.cityRepository = cityRepository;
        this.hotelRepository = hotelRepository;
        this.hotelMapper = hotelMapper;
    }

    public HotelDTO getHotel(String hotelName) {
        return hotelMapper.hotelToHotelDto(hotelRepository.findByName(hotelName).orElseThrow(() -> new HotelNotFoundException("That Hotel do not exist")));
    }

    public List<HotelDTO> findAllHotels() {
        return hotelRepository.findAll().stream().map(hotelMapper::hotelToHotelDto).collect(Collectors.toList());
    }

    public List<HotelDTO> getHotelsByCityName(String cityName) {
        if (cityRepository.findCityByName(cityName).isEmpty()) {
            throw new CityNotFoundException("That City do not exist");
        }
        return hotelRepository.findByCityName(cityName).stream()
                .map(hotelMapper::hotelToHotelDto).collect(Collectors.toList());
    }

    public void addHotel(HotelDTO hotelDTO) {
        Hotel hotel = hotelMapper.hotelDTOtoHotel(hotelDTO);
        hotelRepository.save(hotel);
    }

    public void updateHotel(String hotelName, HotelDTO hotelDTO) {
        Hotel hotelToUpdate = hotelRepository.findByName(hotelName).orElseThrow(() -> new HotelNotFoundException("That hotel do not exist"));
        hotelToUpdate.setName(hotelDTO.getName());
        hotelToUpdate.setAddress(hotelDTO.getAddress());
        hotelToUpdate.setRating(hotelDTO.getRating());
        hotelRepository.save(hotelToUpdate);
    }

    public HotelDTO deleteHotel(String hotelName) {
        Hotel hotelToDelete = hotelRepository.findByName(hotelName).orElseThrow(() -> new HotelNotFoundException("That Hotel do not exist"));
        hotelRepository.delete(hotelToDelete);
        return hotelMapper.hotelToHotelDto(hotelToDelete);
    }


   public List<HotelDTO> findRecommendedHotels(String hotelName){
    return hotelRepository.findAll(Sort.by(Sort.Direction.DESC, "recommended")).stream()
            .map(hotelMapper::hotelToHotelDto).collect(Collectors.toList());
   }
}