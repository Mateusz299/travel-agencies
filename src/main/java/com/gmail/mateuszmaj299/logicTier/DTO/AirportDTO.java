package com.gmail.mateuszmaj299.logicTier.DTO;

import javax.validation.constraints.NotBlank;

public class AirportDTO {

    @NotBlank(message = "This field cannot be empty")
    private String name;
    @NotBlank(message = "This field cannot be empty")
    private String cityName;
    @NotBlank(message = "This field cannot be empty")
    private String address;

    public AirportDTO() {

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public AirportDTO(String name, String cityName, String address) {
        this.name = name;
        this.cityName = cityName;
        this.address = address;
    }
}
