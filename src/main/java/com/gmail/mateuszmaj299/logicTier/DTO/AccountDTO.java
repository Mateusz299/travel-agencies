package com.gmail.mateuszmaj299.logicTier.DTO;

import java.util.List;

public class AccountDTO {
    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public List<String> getRole() {
        return role;
    }

    public void setRole(List<String> role) {
        this.role = role;
    }

    private String customerName;
    private List<String> role;


}
