package com.gmail.mateuszmaj299.logicTier.DTO;

import java.math.BigDecimal;
import java.util.List;

public class ReservationDTO {

    private String name;

    private String hotelName;

    private String cityName;

    private List<String> airports;

    private String countryName;

    private String continentName;
    private Float rating;
    private String description;

    public Float getRating() {
        return rating;
    }

    public void setRating(Float rating) {
        this.rating = rating;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getHotelName() {
        return hotelName;
    }

    public void setHotelName(String hotelName) {
        this.hotelName = hotelName;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public List<String> getAirports() {
        return airports;
    }

    public void setAirports(List<String> airports) {
        this.airports = airports;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String getContinentName() {
        return continentName;
    }

    public void setContinentName(String continentName) {
        this.continentName = continentName;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public ReservationDTO() {
    }

    public ReservationDTO(String name, String hotelName, String cityName, List<String> airports, String countryName, String continentName, BigDecimal price) {
        this.name = name;
        this.hotelName = hotelName;
        this.cityName = cityName;
        this.airports = airports;
        this.countryName = countryName;
        this.continentName = continentName;
        this.price = price;
    }

    private BigDecimal price;
}
