package com.gmail.mateuszmaj299.logicTier.DTO;



import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import java.util.Objects;

public class HotelDTO {
    @JsonIgnore
    private Integer id;
    @NotBlank(message = "This field cannot be empty")
    private String name;
    private String description;
    @NotBlank(message = "This field cannot be empty")
    private String address;
    private String cityName;
    private String image;

    @NotBlank
    @Min(0)
    @Max(10)
    private float rating;

    public HotelDTO() {
    }

    public HotelDTO(Integer id, String name, String description, String address, String cityName, float rating, String image) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.address = address;
        this.cityName = cityName;
        this.rating = rating;
        this.image = image;
    }

    public int getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public float getRating() {
        return rating;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }

    public String getImage(){return image;}

    public void setImage(String image){this.image=image;}
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        HotelDTO hotelDTO = (HotelDTO) o;
        return Objects.equals(id, hotelDTO.id)
                && Objects.equals(name, hotelDTO.name)
                && Objects.equals(description, hotelDTO.description)
                && Objects.equals(address, hotelDTO.address)
                && Objects.equals(cityName, hotelDTO.cityName)
                && Objects.equals(rating, hotelDTO.rating)
                && Objects.equals(image, hotelDTO.image);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, description, address, cityName, rating, image);
    }
}