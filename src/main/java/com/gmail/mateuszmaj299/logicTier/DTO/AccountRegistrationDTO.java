package com.gmail.mateuszmaj299.logicTier.DTO;

import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class AccountRegistrationDTO {
    @NotBlank(message = "Account email address is mandatory")
    private String emailAddress;
    @NotBlank(message = "Password is mandatory")
    @Size(min = 8, max = 32, message = "Password size should be between 8 and 32 characters")
    private String password;

    public AccountRegistrationDTO(String emailAddress, String password) {
        this.emailAddress = emailAddress;
        this.password = password;
    }

    private final Pattern passwordPattern = Pattern.compile("^(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])(?=.*[!@#$%^&*(),.?\":{}|<>])\\S+$");

    @AssertTrue(message = "Password should contain at least one lowercase and one uppercase letter, one digit and can't contain white spaces")
    private boolean isPasswordCorrect() {
        Matcher matcher = passwordPattern.matcher(password);
        return matcher.matches();
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String name) {
        this.emailAddress = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
