package com.gmail.mateuszmaj299.logicTier.DTO;


import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

public class ReservationDemands {

    @NotBlank(message = "This field cannot be empty")
    private Float rating;
    @NotBlank(message = "This field cannot be empty")
    private String description;
    @NotBlank(message = "This field cannot be empty")
    private String offerName;
    @NotBlank(message = "This field cannot be empty")
    private String hotelName;
    @NotBlank(message = "This field cannot be empty")
    private String cityName;

    @NotNull(message = "Price cannot be null")
    @DecimalMin(value = "0.00", message = "Price must be greater than or equal to 0")
    @DecimalMax(value = "99999.99", message = "Price must be less than or equal to 99999.99")
    private BigDecimal price;

    public ReservationDemands(String offerName, String hotelName, String cityName, BigDecimal price) {
        this.offerName = offerName;
        this.hotelName = hotelName;
        this.cityName = cityName;
        this.price = price;

    }

    public String getOfferName() {
        return offerName;
    }

    public void setOfferName(String offerName) {
        this.offerName = offerName;
    }

    public String getHotelName() {
        return hotelName;
    }

    public void setHotelName(String hotelName) {
        this.hotelName = hotelName;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Float getRating() {
        return rating;
    }

    public void setRating(Float rating) {
        this.rating = rating;
    }
}
