document.addEventListener('DOMContentLoaded', function() {
    fetch('/hotels/list')
        .then(response => response.json())
        .then(data => {
            const table = document.getElementById("hotelsTableBody");
            data.forEach(hotel => {
                const row = table.insertRow();
                const nameCell = row.insertCell(0);
                nameCell.textContent = hotel.name;

                const descriptionCell = row.insertCell(1);
                descriptionCell.textContent = hotel.description;

                const addressCell = row.insertCell(2);
                addressCell.textContent = hotel.address;

                const ratingCell = row.insertCell(3);
                ratingCell.textContent = hotel.rating;
            });
        });
});