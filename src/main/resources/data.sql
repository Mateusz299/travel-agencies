INSERT INTO CONTINENT (NAME) VALUES
    ('Europe'),
    ('North America'),
    ('South America'),
    ('Asia'),
    ('Africa'),
    ('Australia');
INSERT INTO COUNTRY (NAME, continent_id) VALUES
    ('Poland', 1),
    ('Mexico', 2),
    ('Brazil', 3),
    ('Norway', 1),
    ('Spain', 1),
    ('Indonesia', 4),
    ('Japan', 4),
    ('Kenya', 5),
    ('Morocco', 5),
    ('New Zeland', 6),
    ('Fiji', 6),
    ('Canada', 2);

INSERT INTO CITY (NAME, country_id) VALUES
    ('Cracow', 1),
    ('Wroclaw', 1),
    ('Warsaw', 1),
    ('Mexico City', 2),
    ('Guadalajara', 2),
    ('Rio de Janeiro', 3),
    ('São Paulo',3),
    ('Oslo',4),
    ('Bergen',4),
    ('Madrid',5),
    ('Barcelona',5),
    ('Jakarta',6),
    ('Bali',6),
    ('Tokio',7),
    ('Kioto',7),
    ('Nairobi',8),
    ('Mombasa',8),
    ('Marrakesh',9),
    ('Casablanca',9),
    ('Auckland',10),
    ('Wellington',10),
    ('Suva',11),
    ('Nadi',11),
    ('Toronto',12),
    ('Vancouver',12);

INSERT INTO AIRPORT (NAME, CITY_ID, ADDRESS) VALUES
    ('Międzynarodowy Port Lotniczy im. Jana Pawła II Kraków-Balice', 1, 'Kraków ul. Jana Pawła 1'),
    ('Port Lotniczy Wrocław S.A.', 2, 'Wrocław ul. Lotnica 2'),
    ('Lotnisko Chopina', 3, 'Warszawa ul. Chopina 3'),
    ('Lotnisko Warszawa Modlin', 4, 'Warszawa ul. Modeliny 4');

INSERT INTO HOTEL (NAME, DESCRIPTION ,ADDRESS,CITY_ID, RATING, IMAGE) VALUES
    ('Hotel Bristol, a Luxury Collection Hotel', 'blablabla','Emilii Plater 49, 00-125',3, 8.4,'H3-1.png'),
    ('InterContinental Warsaw', 'blablabla', 'Krakowskie Przedmieście 42/44, 00-325', 3, 6.6,'H3-2.png'),
    ('Hotel Copernicus', 'blablabla','Leszczynowa 79 20-750', 1, 7.9,'H3-11.png'),
    ('Sheraton Grand Krakow', 'blablabla', 'Al. Jana Pawła II 10a, 30:001', 1,  8.4,'H3-2.png'),
    ('Four Seasons Hotel Mexico City', 'blablabla', 'Av. 16 de Septiembre 82, Cuauhtémoc, Centro, 06000 Ciudad de México', 4, 8.5,'H3-3.png'),
    ('Hyatt Regency Andares Guadalajara', 'blablabla', 'lvd. Puerta de Hierro 5065, Puerta de Hierro, 45116 Zapopan', 5,  8.0,'H3-4.png'),
    ('Fiesta Americana Guadalajara', 'blablabla', 'River Road 23', 5,  7.4,'H3-5.png'),
    ('Belmond Copacabana Palace', 'blablabla', 'Av. Atlântica, 1702 - Copacabana', 6,  8.6,'H3-6.png'),
    ('Santa Teresa Hotel RJ - MGallery', 'blablabla', 'Rua Almirante Alexandrino, 660 - Santa Teresa', 6,  6.5,'H3-7.png'),
    ('Hotel Emiliano São Paulo', 'blablabla', 'Rua Oscar Freire, 384 - Jardins, São Paulo - SP, 01426-001', 7,  8.3,'H3-8.png'),
    ('Palácio Tangará - an Oetker Collection Hotel', 'blablabla', 'Rua Deputado Laércio Corte, 1501 - Panamby, São Paulo - SP, 05706-290', 7,  6.6,'H3-9.png'),
    ('The Thief', 'blablabla', 'Landgangen 1, 0252 Oslo', 8,  7.9,'H3-10.png'),
    ('Grand Hotel Oslo', 'blablabla', 'Karl Johans gate 31, 0159 Oslo', 8,  6.3,'H3-1.png'),
    ('Hotel Oleana', 'blablabla', 'River Island 73', 9,  8.5,'H3-2.png'),
    ('Clarion Hotel Admiral', 'blablabla', ' C. Sundtsgate 9, 5004 Bergen', 9,  7.8,'H3-3.png'),
    ('Gran Meliá Palacio de los Duques', 'blablabla', 'Cuesta de Santo Domingo, 5, 28013 Madrid', 10,  7.8,'H3-4.png'),
    ('Hotel Arts Barcelona', 'blablabla', 'Carrer de la Marina, 19-21, 08005 Barcelona', 11, 7.8,'H3-5.png'),
    ('Mandarin Oriental Jakarta', 'blablabla', 'Jl. M.H. Thamrin, Menteng, Jakarta Pusat, DKI Jakarta 10310', 12,  7.8,'H3-6.png'),
    ('AYANA Resort and Spa, Bali', 'blablabla', 'Jl. Karang Mas Sejahtera, Jimbaran, Bali', 13, 7.8,'H3-7.png'),
    ('Aman Tokyo', 'blablabla', 'Chome-5-6 Otemachi, Chiyoda City, Tokyo 100-0004', 14, 7.8,'H3-8.png'),
    ('The Ritz-Carlton, Kyoto', 'blablabla', 'Midtown Tower, 9-7-1 Akasaka, Minato City, Tokyo 107-6245', 15,  7.8,'H3-9.png'),
    ('Villa Rosa Kempinski', 'blablabla', 'Chiromo Road, P.O Box 14164-00800 Nairobi', 16,  7.8,'H3-10.png'),
    ('Serena Beach Resort and Spa', 'blablabla', 'Mbagathi Ridge, Nairobi', 17,  7.8,'H3-11.png'),
    ('La Mamounia Marrakech','blablabla', 'Avenue Prince Moulay Rachid, Marrakesh', 18,  7.8,'H3-1.png'),
    ('Four Seasons Hotel Casablanca', 'blablabla', 'Rue Abou Abbas El Sebti, Marrakesh', 19,  7.8,'H3-2.png'),
    ('Hotel DeBrett', 'blablabla', '2 High St, Auckland 1010', 20,  7.8,'H3-3.png'),
    ('InterContinental Wellington', 'blablabla', 'West Side 77/203 123-543 Wellington', 21,  7.8,'H3-4.png'),
    ('Grand Pacific Hotel', 'blablabla', '456 Victoria Parade, Suva', 22,  7.8,'H3-5.png'),
    ('The Ritz-Carlton, Toronto', 'blablabla', '181 Wellington St W, Toronto, ON M5V 3G7', 23, 7.8,'H3-6.png'),
    ('Fairmont Pacific Rim', 'blablabla', '123 Waterfront Way, Vancouver, BC V6B 0G8', 23, 7.8,'H3-7.png');

INSERT INTO TRIP (NAME, START_DATE, END_DATE, AIRPORT_ID, HOTEL_ID, DESCRIPTION, PRICE, RECOMMENDATION, PERSONS_NUMBER) VALUES
    ('Serbian trip', '2022-01-01', '2022-01-02', 1, 2, 'River Road 73', 800, 18, 8),
    ('Serbian trip2', '2022-05-01','2022-05-02', 1, 2, 'River Road 73', 700, 18, 8);

INSERT INTO CUSTOMER (FIRST_NAME, LAST_NAME, EMAIL, PASSWORD, PHONE_NUMBER, ADDRESS, CITY, VERIFIED) VALUES
    ('Jan','Kowalski','jank@wp.pl','Jan!@#123', '123456789','123 Kraków','One','YES');

INSERT INTO RESERVATION (USER_ID, TRIP_ID, RATING, DESCRIPTION) VALUES
    (1, 1, 8.5, 'blabla'),
    (1, 1, 8.5, 'blabla1'),
    (1, 1, 8.5, 'blabla2');

INSERT INTO BOOKING(USER_ID, TRIP_ID, PAYMENT_ID) VALUES
    (1,1,1);

INSERT INTO REVIEWS(USER_ID,TRIP_ID,DESCRIPTION) VALUES
    (1,1,'that was great');

INSERT INTO PAYMENT(BOOKING_ID, PAYMENT_DATE, AMOUNT, STATUS) VALUES
    (1,'2022-01-02',100000,'paid');