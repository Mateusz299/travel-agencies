package com.gmail.mateuszmaj299.travelagencies.service;

import com.gmail.mateuszmaj299.dataTier.entity.Customer;
import com.gmail.mateuszmaj299.logicTier.DTO.AccountRegistrationDTO;
import com.gmail.mateuszmaj299.logicTier.repository.CustomerRepository;
import com.gmail.mateuszmaj299.logicTier.service.AccountService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.mockito.Mockito.*;
import static org.junit.Assert.*;


public class AccountServiceTest {

    @Mock
    private CustomerRepository customerRepository;

    @Mock
    private PasswordEncoder passwordEncoder;

    @InjectMocks
    private AccountService accountService;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void shouldRegisterUserSuccessfully() {
        AccountRegistrationDTO accountRegistrationDTO = new AccountRegistrationDTO("test@example.com", "Password123");
        Customer expectedCustomer = new Customer();
        expectedCustomer.setEmailAddress("test@example.com");
        expectedCustomer.setPassword("hashedPassword");

        when(passwordEncoder.encode("Password123")).thenReturn("hashedPassword");
        when(customerRepository.save(any(Customer.class))).thenReturn(expectedCustomer);

        Customer registeredCustomer = accountService.registerUser(accountRegistrationDTO);

        assertNotNull(registeredCustomer);
        assertEquals("test@example.com", registeredCustomer.getEmailAddress());
        assertEquals("hashedPassword", registeredCustomer.getPassword());

        verify(passwordEncoder).encode("Password123");
        verify(customerRepository).save(any(Customer.class));
    }

    @Test
    public void shouldLoadUserByUsername() {
        String email = "test@example.com";
        Customer customer = new Customer();
        customer.setEmailAddress(email);
        customer.setPassword("hashedPassword");

        when(customerRepository.findByEmailAddress(email)).thenReturn(Optional.of(customer));

        UserDetails userDetails = accountService.loadUserByUsername(email);

        assertNotNull(userDetails);
        assertEquals(email, userDetails.getUsername());
        assertEquals("hashedPassword", userDetails.getPassword());
    }

    @Test
    public void testLoadUserByUsernameWithNonExistingUser() {
        when(customerRepository.findByEmailAddress(anyString())).thenReturn(Optional.empty());

        assertThrows(UsernameNotFoundException.class, () -> {
            accountService.loadUserByUsername("nonexisting@email.com");
        });
    }

    @Test
    public void testRegisterUserIsSavedToDatabase() {
        AccountRegistrationDTO registrationDTO = new AccountRegistrationDTO("jan.kowalski@example.com", "SecurePassword123!");

        Customer expectedCustomer = new Customer();
        expectedCustomer.setFirstName("Jan");
        expectedCustomer.setLastName("Kowalski");
        expectedCustomer.setEmailAddress("jan.kowalski@example.com");
        expectedCustomer.setPassword("encodedPassword");

        when(passwordEncoder.encode("SecurePassword123!")).thenReturn("encodedPassword");
        when(customerRepository.save(any(Customer.class))).thenReturn(expectedCustomer);

        Customer savedCustomer = accountService.registerUser(registrationDTO);

        ArgumentCaptor<Customer> customerCaptor = ArgumentCaptor.forClass(Customer.class);
        verify(customerRepository).save(customerCaptor.capture());
        Customer capturedCustomer = customerCaptor.getValue();

        assertNotNull(savedCustomer);
        assertEquals("encodedPassword", capturedCustomer.getPassword());
        assertEquals("jan.kowalski@example.com", capturedCustomer.getEmailAddress());
    }

    @Test
    public void testFindUserByEmail() {
        Customer customer = new Customer();
        customer.setEmailAddress("test@example.com");
        when(customerRepository.findByEmailAddress(anyString())).thenReturn(Optional.of(customer));

        Optional<Customer> foundCustomer = accountService.findUserByEmail("test@example.com");

        assertTrue(foundCustomer.isPresent());
        assertEquals(customer, foundCustomer.get());
    }

    @Test
    public void testUpdateUser() {
        Customer existingCustomer = new Customer();
        existingCustomer.setFirstName("Jan");
        existingCustomer.setLastName("Kowalski");
        existingCustomer.setEmailAddress("jan.kowalski@example.com");
        existingCustomer.setPassword("OldPassword123!");

        Customer updatedDetails = new Customer();
        updatedDetails.setFirstName("Janina");
        updatedDetails.setLastName("Nowak");
        updatedDetails.setEmailAddress("janina.nowak@example.com");
        updatedDetails.setPassword("NewSecurePassword456!");

        when(customerRepository.findById(1)).thenReturn(Optional.of(existingCustomer));
        when(customerRepository.save(any(Customer.class))).thenReturn(updatedDetails);

        Optional<Customer> result = accountService.updateUser(1, updatedDetails);

        assertTrue(result.isPresent());
        assertEquals(updatedDetails, result.get());
        assertEquals("Janina", result.get().getFirstName());
        assertEquals("Nowak", result.get().getLastName());
        assertEquals("janina.nowak@example.com", result.get().getEmailAddress());
    }

    @Test
    public void testFindAllUsers() {
        Customer customer1 = new Customer();
        customer1.setFirstName("Jan");
        customer1.setLastName("Kowalski");
        customer1.setEmailAddress("jan.kowalski@example.com");
        customer1.setPassword("Password123!");

        Customer customer2 = new Customer();
        customer2.setFirstName("Anna");
        customer2.setLastName("Nowak");
        customer2.setEmailAddress("anna.nowak@example.com");
        customer2.setPassword("Password456!");

        List<Customer> customers = Arrays.asList(customer1, customer2);
        when(customerRepository.findAll()).thenReturn(customers);

        List<Customer> result = accountService.findAllUsers();

        assertEquals(2, result.size());
        assertTrue(result.contains(customer1));
        assertTrue(result.contains(customer2));
    }

    @Test
    public void testDeleteUser() {
        Customer customer = new Customer();
        customer.setId(1);
        customer.setFirstName("Jan");
        customer.setLastName("Kowalski");
        customer.setEmailAddress("jan.kowalski@example.com");
        customer.setPassword("Password123!");

        when(customerRepository.findById(1)).thenReturn(Optional.of(customer));
        doNothing().when(customerRepository).delete(any(Customer.class));

        boolean result = accountService.deleteUser(1);

        assertTrue(result);
        verify(customerRepository).delete(customer);
    }

    @Test
    public void testDeleteUserFailure() {
        when(customerRepository.findById(anyInt())).thenReturn(Optional.empty());

        boolean result = accountService.deleteUser(1);

        assertFalse(result);
    }

    @Test
    public void testUpdateUserFailure() {
        when(customerRepository.findById(anyInt())).thenReturn(Optional.empty());

        Optional<Customer> result = accountService.updateUser(1, new Customer());

        assertFalse(result.isPresent());
    }

    @Test
    public void testFindUserByEmailAndId() {
        Customer customer = new Customer();
        customer.setId(1);
        customer.setFirstName("Jan");
        customer.setLastName("Kowalski");
        customer.setEmailAddress("test@example.com");
        customer.setPassword("Password123!");

        when(customerRepository.findByEmailAddress("test@example.com")).thenReturn(Optional.of(customer));
        when(customerRepository.findById(1)).thenReturn(Optional.of(customer));

        Optional<Customer> foundByEmail = accountService.findUserByEmail("test@example.com");
        Optional<Customer> foundById = accountService.findUserById(1);

        assertTrue(foundByEmail.isPresent());
        assertEquals(customer, foundByEmail.get());
        assertTrue(foundById.isPresent());
        assertEquals(customer, foundById.get());
    }
}
