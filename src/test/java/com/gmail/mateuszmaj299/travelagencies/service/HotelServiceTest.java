package com.gmail.mateuszmaj299.travelagencies.service;

import com.gmail.mateuszmaj299.dataTier.entity.City;
import com.gmail.mateuszmaj299.dataTier.entity.Country;
import com.gmail.mateuszmaj299.dataTier.entity.Hotel;
import com.gmail.mateuszmaj299.exceptions.HotelNotFoundException;
import com.gmail.mateuszmaj299.logicTier.DTO.HotelDTO;
import com.gmail.mateuszmaj299.logicTier.repository.CityRepository;
import com.gmail.mateuszmaj299.logicTier.repository.HotelRepository;
import com.gmail.mateuszmaj299.logicTier.service.HotelService;
import com.gmail.mateuszmaj299.logicTier.mapper.HotelMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class HotelServiceTest {

    @InjectMocks
    private HotelService hotelService;
    @Mock
    private HotelRepository hotelRepository;
    @Mock
    private HotelMapper hotelMapper;
    @Mock
    private CityRepository cityRepository;

    public HotelDTO hotelDTO1 = new HotelDTO(1, "hotelDTOName1", "Description1", "Address1", "aa", 5.5f,"image.jng");
    public HotelDTO hotelDTO2 = new HotelDTO(2, "hotelDTOName2", "Description2", "Address2", "aa", 5.4f,"image.jng");
    public HotelDTO hotelDTO3 = new HotelDTO(3, "hotelDTOName3", "Description3", "Address3", "bb", 5.3f,"image.jng");
    public HotelDTO hotelDTO4 = new HotelDTO(4, "hotelDTOName4", "Description4", "Address4", "bb", 5.2f,"image.jng");

    public Hotel hotel1 = new Hotel("hotelName1", "Address1",5.0f );
    public Hotel hotel2 = new Hotel("hotelName2", "Address2",4.5f );
    public Hotel hotel3 = new Hotel("hotelName3", "Address3",4.5f );
    public Hotel hotel4 = new Hotel("hotelName4", "Address4",4.0f );


    @Test
    void should_return_all_hotels_from_repository() {
        when(hotelRepository.findAll()).thenReturn(getAllHotels());
        final List<HotelDTO> allHotels = hotelService.findAllHotels();
        assertThat(allHotels)
                .isNotEmpty()
                .hasSize(4);
    }

    @Test
    void should_find_hotel_by_name() {
        String hotelName = "hotelName1";
        when(hotelRepository.findByName(hotelName)).thenReturn(
                Optional.of(getAllHotels().stream().filter(hotel -> hotel.getName().equals(hotelName)).findFirst().orElseThrow()));
        when(hotelMapper.hotelToHotelDto(any(Hotel.class))).thenReturn(hotelDTO1);
        HotelDTO hotelByName = hotelService.getHotel(hotelName);
        assertThat(hotelByName)
                .isEqualTo(hotelDTO1);
    }

    @Test
    void should_return_optional_empty_when_hotel_was_not_found_by_name() {}
    @Test
    void testGetHotelsByCityName() {
        Hotel hotel = new Hotel();
        hotel.setName("Grand Hotel");
        when(cityRepository.findCityByName(anyString())).thenReturn(Optional.of(new City(hotel.getName())));
        List<Hotel> hotels = List.of(new Hotel(), new Hotel());
        when(hotelRepository.findByCityName(anyString())).thenReturn(hotels);
        List<HotelDTO> expected = hotels.stream().map(hotelMapper::hotelToHotelDto).toList();
        when(hotelMapper.hotelToHotelDto(any(Hotel.class))).thenReturn(new HotelDTO());
        List<HotelDTO> result = hotelService.getHotelsByCityName("Test City");
        assertThat(result).hasSize(expected.size());
    }

    @Test
    void should_return_empty_optional_when_hotel_not_found_in_database() {
        String hotelName = "hotelName1";
        when(hotelRepository.findByName(hotelName)).thenReturn(Optional.empty());

        assertThrows(HotelNotFoundException.class, () -> hotelService.deleteHotel(hotelName));

        verify(hotelRepository).findByName(hotelName);
        verifyNoMoreInteractions(hotelRepository);
    }

    private List<HotelDTO> getAllHotelsDTO() {
        return Arrays.asList(
                hotelDTO1, hotelDTO2, hotelDTO3, hotelDTO4
        );
    }

    private List<Hotel> getAllHotels() {
        return Arrays.asList(
                hotel1, hotel2, hotel3, hotel4
        );
    }

}