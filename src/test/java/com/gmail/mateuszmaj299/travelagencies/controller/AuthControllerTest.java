package com.gmail.mateuszmaj299.travelagencies.controller;

import com.gmail.mateuszmaj299.dataTier.entity.Customer;
import com.gmail.mateuszmaj299.logicTier.DTO.AccountRegistrationDTO;
import com.gmail.mateuszmaj299.logicTier.service.AccountService;
import com.gmail.mateuszmaj299.presentationTier.controller.AuthController;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.Authentication;

import java.util.Map;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;
import static org.springframework.http.HttpStatus.*;


@ExtendWith(MockitoExtension.class)
class AuthControllerTest {

    @Mock
    private AccountService accountService;

    @Mock
    private AuthenticationManager authenticationManager;

    @InjectMocks
    private AuthController authController;

    @Test
    void loginUser_Success() {
        AccountRegistrationDTO dto = new AccountRegistrationDTO("user@example.com", "Password123!");
        when(authenticationManager.authenticate(any())).thenReturn(mock(Authentication.class));

        ResponseEntity<?> response = authController.loginUser(dto);

        assertEquals(OK, response.getStatusCode());
        assertEquals("User authenticated successfully", response.getBody());
    }

    @Test
    void loginUser_Failure() {
        AccountRegistrationDTO dto = new AccountRegistrationDTO("user@example.com", "wrongPassword");
        when(authenticationManager.authenticate(any())).thenThrow(BadCredentialsException.class);

        ResponseEntity<?> response = authController.loginUser(dto);

        assertEquals(UNAUTHORIZED, response.getStatusCode());
    }

    @Test
    void registerUser_Success() {
        AccountRegistrationDTO dto = new AccountRegistrationDTO("newuser@example.com", "Password123!");
        when(accountService.findUserByEmail(anyString())).thenReturn(Optional.empty());

        ResponseEntity<?> response = authController.registerUser(dto);

        assertEquals(OK, response.getStatusCode());
        assertEquals(Map.of("status", "success", "message", "User registered successfully"), response.getBody());
    }

    @Test
    void registerUser_EmailInUse() {
        AccountRegistrationDTO dto = new AccountRegistrationDTO("user@example.com", "Password123!");
        when(accountService.findUserByEmail(anyString())).thenReturn(Optional.of(mock(Customer.class)));

        ResponseEntity<?> response = authController.registerUser(dto);

        assertEquals(BAD_REQUEST, response.getStatusCode());
        assertEquals(Map.of("message", "Email already in use"), response.getBody());
    }
}