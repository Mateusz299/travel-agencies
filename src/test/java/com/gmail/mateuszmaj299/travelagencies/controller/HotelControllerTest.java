package com.gmail.mateuszmaj299.travelagencies.controller;

import static org.mockito.Mockito.*;
import static org.junit.Assert.*;

import com.gmail.mateuszmaj299.exceptions.CityNotFoundException;
import com.gmail.mateuszmaj299.exceptions.HotelNotFoundException;
import com.gmail.mateuszmaj299.logicTier.DTO.HotelDTO;
import com.gmail.mateuszmaj299.presentationTier.controller.HotelController;
import com.gmail.mateuszmaj299.logicTier.service.HotelService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import org.springframework.ui.Model;


import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class HotelControllerTest {

    @Mock
    private HotelService hotelService;
    @Mock
    private Model model;

    @InjectMocks
    private HotelController hotelController;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testGetHotel() {
        String hotelName = "TestHotel";
        HotelDTO mockHotelDTO = new HotelDTO(1, hotelName, "Description", "Address", "aa", 5.0f, "image.jng");
        when(hotelService.getHotel(hotelName)).thenReturn(mockHotelDTO);

        HotelDTO result = hotelController.getHotel(hotelName);

        verify(hotelService).getHotel(hotelName);
        assertNotNull(result);
        assertEquals(hotelName, result.getName());
    }

    @Test(expected = HotelNotFoundException.class)
    public void testGetHotel_NonExistent() {
        String nonExistentHotelName = "NonExistentHotel";
        when(hotelService.getHotel(nonExistentHotelName)).thenThrow(new HotelNotFoundException("That Hotel do not exist"));

        hotelController.getHotel(nonExistentHotelName);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetHotel_InvalidInput() {
        String invalidHotelName = "";
        when(hotelService.getHotel(invalidHotelName)).thenThrow(new IllegalArgumentException("Invalid hotel name"));

        hotelController.getHotel(invalidHotelName);
    }

    @Test
    public void testGetHotel_VerifyOutput() {
        String hotelName = "TestHotel";
        HotelDTO mockHotelDTO = new HotelDTO(1, hotelName, "Description", "Address", "aa", 5.0f,"image.jng");
        when(hotelService.getHotel(hotelName)).thenReturn(mockHotelDTO);

        HotelDTO result = hotelController.getHotel(hotelName);

        assertNotNull(result);
        assertEquals(mockHotelDTO.getId(), result.getId());
        assertEquals(mockHotelDTO.getName(), result.getName());
        assertEquals(mockHotelDTO.getDescription(), result.getDescription());
        assertEquals(mockHotelDTO.getAddress(), result.getAddress());
        assertEquals(mockHotelDTO.getCityName(), result.getCityName());
        assertEquals(mockHotelDTO.getRating(), result.getRating(), 0.0);
    }

    @Test
    public void testListHotels() {
        List<HotelDTO> hotelList = Arrays.asList(new HotelDTO(1, "Hotel1", "Description1", "Address1", "aa", 5.0f,"image.jng"),
                new HotelDTO(2, "Hotel2", "Description2", "Address2", "aa", 4.0f,"image.jng"));
        when(hotelService.findAllHotels()).thenReturn(hotelList);
        ResponseEntity<List<HotelDTO>> response = hotelController.listHotels();
        verify(hotelService).findAllHotels();
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(hotelList, response.getBody());
    }

    @Test
    public void testListHotels_EmptyList() {
        when(hotelService.findAllHotels()).thenReturn(Collections.emptyList());
        ResponseEntity<List<HotelDTO>> response = hotelController.listHotels();
        verify(hotelService).findAllHotels();
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(Collections.emptyList(), response.getBody());
    }

    @Test
    public void testGetHotelsByCityName_Success() {
        String cityName = "TestCity";
        List<HotelDTO> expectedHotels = Arrays.asList(
                new HotelDTO(1, "Hotel1", "Description1", "Address1", "aa", 5.0f,"image.jng"),
                new HotelDTO(2, "Hotel2", "Description2", "Address2", "aa", 4.5f,"image.jng")
        );

        when(hotelService.getHotelsByCityName(cityName)).thenReturn(expectedHotels);

        List<HotelDTO> actualHotels = hotelController.getHotelsByCityName(cityName);

        verify(hotelService).getHotelsByCityName(cityName);
        assertEquals(expectedHotels, actualHotels);
    }

    @Test
    public void testGetHotelsByCityName_NoHotelsFound() {
        String cityName = "NonExistentCity";
        when(hotelService.getHotelsByCityName(cityName)).thenReturn(Collections.emptyList());

        List<HotelDTO> actualHotels = hotelController.getHotelsByCityName(cityName);

        verify(hotelService).getHotelsByCityName(cityName);
        assertTrue(actualHotels.isEmpty());
    }

    @Test(expected = CityNotFoundException.class)
    public void testGetHotelsByCityName_CityNotFound() {
        String nonExistentCityName = "NonExistentCity";
        when(hotelService.getHotelsByCityName(nonExistentCityName)).thenThrow(new CityNotFoundException("That City do not exist"));

        hotelController.getHotelsByCityName(nonExistentCityName);
    }

    @Test(expected = HotelNotFoundException.class)
    public void testDeleteHotel_NonExistent() {
        String nonExistentHotelName = "NonExistentHotel";
        doThrow(new HotelNotFoundException("That Hotel do not exist")).when(hotelService).deleteHotel(nonExistentHotelName);

        hotelController.deleteHotel(nonExistentHotelName);
    }

    @Test
    public void testDeleteHotel_Success() {
        String hotelName = "ExistingHotel";
        HotelDTO mockHotelDTO = new HotelDTO(1, hotelName, "Description", "Address", "aa", 5.0f,"image.jng");
        when(hotelService.deleteHotel(hotelName)).thenReturn(mockHotelDTO);

        ResponseEntity<HotelDTO> response = hotelController.deleteHotel(hotelName);

        verify(hotelService).deleteHotel(hotelName);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(mockHotelDTO, response.getBody());
    }

    @Test(expected = HotelNotFoundException.class)
    public void testUpdateHotel_NonExistent() {
        String nonExistentHotelName = "NonExistentHotel";
        HotelDTO hotelDTO = new HotelDTO(1, nonExistentHotelName, "Description", "Address", "aa", 5.0f,"image.jng");
        doThrow(new HotelNotFoundException("That Hotel do not exist")).when(hotelService).updateHotel(nonExistentHotelName, hotelDTO);

        hotelController.updateHotel(nonExistentHotelName, hotelDTO);
    }

    @Test
    public void testUpdateHotel_Success() {
        String hotelName = "ExistingHotel";
        HotelDTO hotelDTO = new HotelDTO(1, hotelName, "Description", "Address", "aa", 5.0f,"image.jng");
        doNothing().when(hotelService).updateHotel(hotelName, hotelDTO);

        ResponseEntity<HotelDTO> response = hotelController.updateHotel(hotelName, hotelDTO);

        verify(hotelService).updateHotel(hotelName, hotelDTO);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(hotelDTO, response.getBody());
    }

}