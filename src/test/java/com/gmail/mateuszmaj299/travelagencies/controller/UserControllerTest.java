package com.gmail.mateuszmaj299.travelagencies.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gmail.mateuszmaj299.dataTier.entity.Customer;
import com.gmail.mateuszmaj299.logicTier.service.AccountService;
import com.gmail.mateuszmaj299.presentationTier.controller.UserController;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;
import java.util.Optional;

import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(UserController.class)
@ActiveProfiles("test")
class UserControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private AccountService accountService;

    @MockBean
    PasswordEncoder passwordEncoder;

    @Autowired
    private ObjectMapper objectMapper;

    private Customer sampleUser;

    @BeforeEach
    void setUp() {
        sampleUser = new Customer();
        sampleUser.setId(1);
        sampleUser.setFirstName("Jan");
        sampleUser.setLastName("Kowalski");
        sampleUser.setEmailAddress("jan.kowalski@example.com");
        sampleUser.setPassword("password");
        sampleUser.setRole("USER");
    }

    @WithMockUser(roles = "USER")
    @Test
    void getAllUsers_ShouldReturnListOfUsers() throws Exception {
        given(accountService.findAllUsers()).willReturn(Arrays.asList(sampleUser));

        mockMvc.perform(get("/api/users")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].firstName").value(sampleUser.getFirstName()));
    }

    @WithMockUser(roles = "USER")
    @Test
    void getUserById_ShouldReturnUser() throws Exception {
        given(accountService.findUserById(1)).willReturn(Optional.of(sampleUser));

        mockMvc.perform(get("/api/users/{id}", 1)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.firstName").value(sampleUser.getFirstName()));
    }

    @WithMockUser(roles = "USER")
    @Test
    void getUserById_NotFound() throws Exception {
        given(accountService.findUserById(1)).willReturn(Optional.empty());

        mockMvc.perform(get("/api/users/{id}", 1)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @WithMockUser(roles = "USER")
    @Test
    void updateUser_ShouldUpdateUser() throws Exception {
        Customer updatedUser = new Customer();
        updatedUser.setFirstName("Janek");
        updatedUser.setLastName("Nowak");

        given(accountService.updateUser(1, sampleUser)).willReturn(Optional.of(updatedUser));

        mockMvc.perform(put("/api/users/{id}", 1)

                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(sampleUser)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.firstName").value(updatedUser.getFirstName()));
    }

    @Test
    void deleteUser_ShouldDeleteUser() throws Exception {
        given(accountService.deleteUser(1)).willReturn(true);

        mockMvc.perform(delete("/api/users/{id}", 1)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    void deleteUser_NotFound() throws Exception {
        given(accountService.deleteUser(1)).willReturn(false);

        mockMvc.perform(delete("/api/users/{id}", 1)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }
}