package com.gmail.mateuszmaj299.travelagencies.controller;

import static org.mockito.Mockito.*;
import static org.junit.Assert.*;

import com.gmail.mateuszmaj299.logicTier.DTO.AirportDTO;
import com.gmail.mateuszmaj299.presentationTier.controller.AirportController;
import com.gmail.mateuszmaj299.logicTier.service.AirportService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.servlet.ModelAndView;

import java.util.Arrays;
import java.util.List;

public class AirportControllerTest {

    @Mock
    private AirportService airportService;

    @InjectMocks
    private AirportController airportController;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testGetAllAirports() {
        AirportDTO airport1 = new AirportDTO("Airport1", "City1", "Address1");
        AirportDTO airport2 = new AirportDTO("Airport2", "City2", "Address2");
        when(airportService.getAllAirports()).thenReturn(Arrays.asList(airport1, airport2));

        ModelAndView modelAndView = airportController.getAllAirports();

        verify(airportService).getAllAirports();
        assertEquals("airports", modelAndView.getViewName());

        List<AirportDTO> result = (List<AirportDTO>) modelAndView.getModel().get("airports");
        assertEquals(2, result.size());
    }
    @Test
    public void testAddAirport() {
        AirportDTO newAirport = new AirportDTO("NewAirport", "NewCity", "NewAddress");
        doNothing().when(airportService).addAirport(any(AirportDTO.class));

        ResponseEntity<AirportDTO> response = airportController.addAirport(newAirport);

        verify(airportService).addAirport(refEq(newAirport));
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(newAirport, response.getBody());
    }
    @Test
    public void testDeleteAirport() {
        String airportName = "ExistingAirport";
        AirportDTO existingAirport = new AirportDTO(airportName, "City", "Address");
        when(airportService.deleteAirport(airportName)).thenReturn(existingAirport);

        ResponseEntity<AirportDTO> response = airportController.deleteAirport(airportName);

        verify(airportService).deleteAirport(airportName);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(existingAirport, response.getBody());
    }
}