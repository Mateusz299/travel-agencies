package com.gmail.mateuszmaj299.travelagencies.repository;

import com.gmail.mateuszmaj299.dataTier.entity.City;
import com.gmail.mateuszmaj299.dataTier.entity.Hotel;
import com.gmail.mateuszmaj299.logicTier.repository.CityRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertFalse;

@DataJpaTest
public class CityRepositoryTest {
    @Autowired
    private CityRepository cityRepository;

    private City city;

    @BeforeEach
    void setUp(){
        city = new City("cityName");
    }

    @Test
    void givenCityObject_WhenSave_ThenReturnedSavedCity() {
        City savedCity = cityRepository.save(city);
        assertThat(savedCity).isNotNull();
        assertThat(savedCity.getId()).isGreaterThan(0);
    }

    @Test
    void givenCity_WhenFindByName_ThenReturnCity_Positive() {
        cityRepository.save(city);

        City city1 = cityRepository.findCityByName(city.getName()).get();

        assertThat(city1).isNotNull();
    }
    @Test
    void givenCity_WhenFindByName_ThenReturnCity_Negative() {
        cityRepository.save(city);

        Optional<City> city1 = cityRepository.findCityByName("NonexistentCity");

        assertFalse(city1.isPresent());


    }

    @DisplayName("JUnit test for finding city by ID")
    @Test
    void givenCity_WhenFindById_ThenReturnCity() {
        cityRepository.save(city);

        City city1 = cityRepository.findById(city.getId()).get();

        assertThat(city1).isNotNull();
    }

    @Test
    void givenCity_WhenUpdatedCity_ThenFieldIsUpdated() {
        cityRepository.save(city);

        City actualCity = cityRepository.findById(city.getId()).get();
        actualCity.setName("CityName2updated");
        City updatedCity = cityRepository.save(actualCity);

        assertThat(updatedCity.getName()).isEqualTo("CityName2updated");
    }


}
