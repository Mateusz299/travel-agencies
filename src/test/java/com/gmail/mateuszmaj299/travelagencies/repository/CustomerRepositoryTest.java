//package com.gmail.mateuszmaj299.travelagencies.repository;
//
//import com.gmail.mateuszmaj299.dataTier.entity.Customer;
//import com.gmail.mateuszmaj299.logicTier.repository.CustomerRepository;
//import org.junit.After;
//import org.junit.Before;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
//import org.springframework.test.context.junit4.SpringRunner;
//import org.springframework.transaction.annotation.Propagation;
//import org.springframework.transaction.annotation.Transactional;
//
//import java.util.Optional;
//
//import static org.assertj.core.api.Assertions.assertThat;
//
//
//@Transactional(propagation = Propagation.REQUIRED)
//@RunWith(SpringRunner.class)
//@SpringBootTest
//public class CustomerRepositoryTest {
//
//    @Autowired
//    private CustomerRepository customerRepository;
//
//    private Customer customer;
//
//    @Before
//    public void setUp() {
//        customer = new Customer();
//        customer.setEmailAddress("jan@test.com");
//        customer = customerRepository.save(customer);
//    }
//
//    @After
//    public void tearDown() {
//        customerRepository.deleteAll();
//    }
//
//    @Test
//    public void testFindByEmailAddress() {
//        Optional<Customer> result = customerRepository.findByEmailAddress(customer.getEmailAddress());
//        assertThat(result).isPresent();
//        assertThat(result.get().getEmailAddress()).isEqualTo(customer.getEmailAddress());
//    }
//
//    @Test
//    public void testFindById() {
//        Optional<Customer> result = customerRepository.findById(customer.getId());
//        assertThat(result).isPresent();
//        assertThat(result.get().getEmailAddress()).isEqualTo(customer.getEmailAddress());
//    }
//}