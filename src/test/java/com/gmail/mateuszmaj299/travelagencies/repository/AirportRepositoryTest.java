package com.gmail.mateuszmaj299.travelagencies.repository;

import com.gmail.mateuszmaj299.dataTier.entity.Airport;
import com.gmail.mateuszmaj299.dataTier.entity.City;
import com.gmail.mateuszmaj299.logicTier.repository.AirportRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest
class AirportRepositoryTest {

    @Autowired
    private AirportRepository airportRepository;

    private Airport airport;
    private City city;

    @BeforeEach
    void setUp() {
        city = new City("CityName1");
        airport = new Airport("AirportName1", city);
    }

    @Test
    void givenAirportObject_WhenSave_ThenReturnSavedAirport() {
        Airport savedAirport = airportRepository.save(airport);
        assertThat(savedAirport).isNotNull();
        assertThat(savedAirport.getID()).isGreaterThan(0);
    }

    @Test
    void givenAirportsList_WhenFindAll_ReturnAirportsList() {
        Airport airport1 = new Airport("AirportName2", new City("CityName2"));
        airportRepository.save(airport);
        airportRepository.save(airport1);

        List<Airport> airportList = airportRepository.findAll();

        assertThat(airportList).isNotNull();
        assertThat(airportList.size()).isGreaterThanOrEqualTo(2);
    }

    @Test
    void givenAirport_WhenFindByNameIgnoreCase_ThenReturnAirport() {
        airportRepository.save(airport);

        Optional<Airport> foundAirport = airportRepository.findByName("airportname1");

        assertThat(foundAirport).isPresent();
        assertThat(foundAirport.get().getName()).isEqualToIgnoringCase(airport.getName());
    }

    @Test
    void givenAirports_WhenFindByCityName_ReturnAirportsList() {
        airportRepository.save(airport);

        List<Airport> airportsByCity = airportRepository.findByCityNameIgnoreCase("cityname1");

        assertThat(airportsByCity).isNotEmpty();
        assertThat(airportsByCity.get(0).getCity().getName()).isEqualToIgnoringCase(city.getName());
    }
}