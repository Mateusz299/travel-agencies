package com.gmail.mateuszmaj299.travelagencies.repository;

import com.gmail.mateuszmaj299.dataTier.entity.Hotel;
import com.gmail.mateuszmaj299.logicTier.repository.HotelRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest
class HotelRepositoryTest {
    @Autowired
    private HotelRepository hotelRepository;
    private Hotel hotel;

    @BeforeEach
    void setUp(){
        hotel = new Hotel("hotelName1", "Address1",5.0f );
    }

    @Test
    void givenHotelObject_WhenSave_ThenReturnedSavedHotel() {
        Hotel savedHotel = hotelRepository.save(hotel);
        assertThat(savedHotel).isNotNull();
        assertThat(savedHotel.getId()).isGreaterThan(0);
    }

    @Test
    void givenHotelsList_WhenFindAll_ThenReturnedHotelsList() {
        Hotel hotel1 = new Hotel("hotelName2", "Address2",6.0f );
        int startHotelsNumber = hotelRepository.findAll().size();
        hotelRepository.save(hotel);
        hotelRepository.save(hotel1);

        List<Hotel> hotelList = hotelRepository.findAll();

        assertThat(hotelList).isNotNull();
        assertThat(hotelList.size()).isEqualTo( startHotelsNumber + 2);
    }

    @DisplayName("JUnit test for finding hotel by ID")
    @Test
    void givenHotel_WhenFindById_ThenReturnHotel() {
        hotelRepository.save(hotel);

        Hotel hotelDB = hotelRepository.findById(hotel.getId()).get();

        assertThat(hotelDB).isNotNull();
    }

    @Test
    void givenHotel_WhenFindByName_ThenReturnHotel() {
        hotelRepository.save(hotel);

        Hotel hotelDB = hotelRepository.findByName(hotel.getName()).get();

        assertThat(hotelDB).isNotNull();
    }

    @Test
    void givenHotel_WhenUpdatedHotel_ThenFieldIsUpdated() {
        hotelRepository.save(hotel);

        Hotel actualHotel = hotelRepository.findById(hotel.getId()).get();
        actualHotel.setName("hotelName2updated");
        Hotel updatedHotel = hotelRepository.save(actualHotel);

        assertThat(updatedHotel.getName()).isEqualTo("hotelName2updated");
    }

    @Test
    void givenHotel_WhenDeleted_ThenReturnEmptyOptional() {
        hotelRepository.save(hotel);

        hotelRepository.deleteById(hotel.getId());
        Optional<Hotel> hotelOptional = hotelRepository.findById(hotel.getId());
        assertThat(hotelOptional).isEmpty();
    }

    @Test
    void givenHotel_WhenFindByNameCamelCase_ThenReturnEmptyOptional(){
        hotelRepository.save(hotel);
        String camelCaseHotelName = "hOTelNAMe1";

        Hotel hotelDB = hotelRepository.findByName(camelCaseHotelName).get();

        assertThat(hotelDB).isEqualTo(hotel);
    }

    @Test
    void givenHotel_WhenFindByCityNameCamelCase_ThenReturnEmptyOptional(){
        hotelRepository.save(hotel);
        String camelCaseHotelName = "hOTelNAMe1";

        Hotel hotelDB = hotelRepository.findByName(camelCaseHotelName).get();

        assertThat(hotelDB).isEqualTo(hotel);
    }
}