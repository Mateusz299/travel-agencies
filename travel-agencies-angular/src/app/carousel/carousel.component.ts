import {Component, inject, Input, OnInit} from '@angular/core';
import {HttpClientModule} from '@angular/common/http';
import {SpringService} from "../spring.service";

import { Storage, getDownloadURL, ref } from '@angular/fire/storage';
import {Hotel} from "../hotel";

@Component({
  selector: 'app-carousel',
  standalone: true,
  imports: [HttpClientModule],
  templateUrl: './carousel.component.html',
  styleUrl: './carousel.component.css'
})
export class CarouselComponent implements OnInit {
  @Input() carouselId: string | undefined;
  hotels: Hotel[]=[];

  downloadURL: string []= [];
  private storage: Storage;

  constructor(private springService: SpringService) {
    this.storage = inject(Storage);
  }
  ngOnInit() {
    this.springService.getHotels().subscribe(data => {
      console.log(data);
      this.hotels = data;
      for (let i=0; i<=2;i++){
        this.downloadfile(this.hotels[i].image, i);
      }
    });
  }
  async downloadfile (image: string, i:number ){
    const pathReference  = ref(this.storage,image);
    try {
      this.downloadURL[i] = await getDownloadURL(pathReference);
    } catch (error) {
      console.error('Błąd podczas pobierania URL obrazka:', error);
    }
  }
}
