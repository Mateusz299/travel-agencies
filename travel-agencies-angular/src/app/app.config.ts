import {ApplicationConfig, importProvidersFrom} from '@angular/core';
import { provideRouter } from '@angular/router';

import { routes } from './app.routes';
import { provideHttpClient } from '@angular/common/http';
import {initializeApp, provideFirebaseApp} from "@angular/fire/app";
import {getStorage, provideStorage} from "@angular/fire/storage";

const firebaseConfig = {
  apiKey: "AIzaSyAtz7E3tawkyRs73RYAuDuNtsktORJsMrM",
  authDomain: "travelagencies-7f603.firebaseapp.com",
  projectId: "travelagencies-7f603",
  storageBucket: "travelagencies-7f603.appspot.com",
  messagingSenderId: "612330149757",
  appId: "1:612330149757:web:be24e78a2cce8a1c59de14",
  measurementId: "G-YFPNSMNJ6X"
};


export const appConfig: ApplicationConfig = {
  providers: [provideRouter(routes),provideHttpClient(), importProvidersFrom([
    provideFirebaseApp(()=>initializeApp(firebaseConfig)),
    provideStorage(()=>getStorage())
  ])]
};
