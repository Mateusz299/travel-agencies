import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Hotel} from "./hotel";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class SpringService {

  springServer : string ="http://localhost:8080/api/";
  constructor(private httpClient : HttpClient) { }  //https://www.itsolutionstuff.com/post/angular-17-httpclient-http-services-tutorialexample.html

  getHotels():Observable<Hotel[]>{
    return this.httpClient.get<Hotel[]>("http://localhost:8080/hotels/list")
  }
}
