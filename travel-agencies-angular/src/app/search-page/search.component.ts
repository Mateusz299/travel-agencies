import {Component, OnInit} from '@angular/core';
import { CommonModule } from '@angular/common';
import {FooterComponent} from "../footer/footer.component";
import {ActivatedRoute} from "@angular/router";
import {SpringService} from "../spring.service";
import {Hotel} from "../hotel";
import {SearchBarComponent} from "../search-bar/search-bar.component";

@Component({
  selector: 'app-search',
  standalone: true,
  imports: [
    FooterComponent,
    SearchBarComponent,
    CommonModule
  ],
  templateUrl: './search.component.html',
  styleUrl: './search.component.css'
})
export class SearchComponent implements OnInit{
  city:string|null =null;
  hotels: Hotel[] =[];

  constructor(private route: ActivatedRoute, private springService: SpringService) {}
  ngOnInit(): void {
    this.city = this.route.snapshot.paramMap.get('city');
    this.springService.getHotels().subscribe(data => {
      this.hotels = data;
      console.log(data);
      console.log(this.city);
    if (this.city){
        this.hotels=this.findHotelByCity(this.city, data);
    }
    });
  }
  findHotelByCity(city: string, hotels: Hotel[] ): Hotel[]  {
    return hotels.filter(hotel => hotel.cityName.toLowerCase() === city.toLowerCase());
  }
}
