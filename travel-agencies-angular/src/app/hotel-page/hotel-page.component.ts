import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {SpringService} from "../spring.service";
import {Hotel} from "../hotel";

@Component({
  selector: 'app-hotel-page',
  standalone: true,
  imports: [],
  templateUrl: './hotel-page.component.html',
  styleUrl: './hotel-page.component.css'
})
export class HotelPageComponent implements OnInit {
  hotelName:string |null =null;
  hotels: Hotel[]=[];
  hotel: Hotel | undefined ;
  constructor(private route: ActivatedRoute, private springService: SpringService) {}

  ngOnInit(): void {
    this.hotelName = this.route.snapshot.paramMap.get('hotelName');
    if (this.hotelName) {
    this.springService.getHotels().subscribe(data => {
      this.hotels= data;
      this.hotel=this.findHotelByName(this.hotelName);
    });
    }
  }

  findHotelByName(name: string | null): Hotel | undefined {
    return this.hotels.find(hotel => hotel.name === name);
  }
}
