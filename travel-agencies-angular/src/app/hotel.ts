export interface Hotel{
  cityName: string;
  name: string;
  description: string;
  address: string;
  rating: number;
  image: string;
}
