import { Component } from '@angular/core';
import {CarouselComponent} from "../carousel/carousel.component";
import {LogoComponent} from "../logo/logo.component";
import {NavigationComponent} from "../navigation/navigation.component";
import {SearchBarComponent} from "../search-bar/search-bar.component";
import {FooterComponent} from "../footer/footer.component";

@Component({
  selector: 'app-start-page',
  standalone: true,
  imports: [
    CarouselComponent,
    LogoComponent,
    NavigationComponent,
    SearchBarComponent,
    FooterComponent
  ],
  templateUrl: './start-page.component.html',
  styleUrl: './start-page.component.css'
})
export class StartPageComponent {

}
