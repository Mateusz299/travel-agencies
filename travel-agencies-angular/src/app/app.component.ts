import {Component} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterOutlet} from '@angular/router';
import {NavigationComponent} from "./navigation/navigation.component";
import {LogoComponent} from "./logo/logo.component";
import {CarouselComponent} from "./carousel/carousel.component";
import {SearchBarComponent} from "./search-bar/search-bar.component";
import {SpringService} from "./spring.service";
import {Hotel} from "./hotel";
import {HttpClientModule} from "@angular/common/http";
import {StartPageComponent} from "./start-page/start-page.component";


@Component({
    selector: 'app-root',
    standalone: true,
  imports: [CommonModule, RouterOutlet, NavigationComponent, LogoComponent, CarouselComponent, SearchBarComponent, HttpClientModule, StartPageComponent],
    templateUrl: './app.component.html',
    styleUrl: './app.component.css'
})
export class AppComponent {
    title = 'travel-agencies-angular';
}
