import { Component } from '@angular/core';
import { Router } from '@angular/router';
import {HttpClientModule} from "@angular/common/http";
import { FormsModule } from '@angular/forms';

@Component({
  selector: 'app-search-bar',
  standalone: true,
  imports: [FormsModule],
  templateUrl: './search-bar.component.html',
  styleUrl: './search-bar.component.css'
})
export class SearchBarComponent {
  city: string = '';
  startDate: string = '';
  endDate: string = '';
  guestsNumber: number | null = null;
  constructor(private router: Router) { }
  search() {
    if (this.city) {
      this.router.navigate(['/search', this.city]);
    }else {this.router.navigate(['/search']);
    }
  }
}
