import { Routes } from '@angular/router';
import {AboutUsComponent} from "./about-us/about-us.component";
import {SearchComponent} from "./search-page/search.component";
import {ContactComponent} from "./contact/contact.component";
import {StartPageComponent} from "./start-page/start-page.component";
import {HotelPageComponent} from "./hotel-page/hotel-page.component";

export const routes: Routes = [
  {path:"", component: StartPageComponent},
  {path:"about-us", component: AboutUsComponent},
  {path:"search/:city", component: SearchComponent},
  {path:"search", component: SearchComponent},
  {path:"contact", component: ContactComponent},
  {path:"hotel/:hotelName", component: HotelPageComponent},
];
