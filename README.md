# Travel Agencies Management System

## Overview

This Travel Agencies Management System is an all-in-one app that helps travel agencies run their business smoothly. It lets people sign up, log in, manage customer info, and handle travel deals. The app is easy to use for both the people running the agency and their customers, making everything from booking to managing trips simpler and more efficient.
## Features

### User Authentication:
Secure login and registration functionalities for both administrators and clients.
### Customer Management: 
Administrators can add, update, and delete customer information of client details.
### Travel Packages: 
Ability to create, update, and manage various travel packages, including destinations, pricing, and availability.
### Booking System: 
In progress.
### Security: 
Implemented robust security measures, including encrypted passwords and role-based access control, to protect sensitive information.

## Technologies Used

- Spring Boot: For creating the backend service with minimal configuration.
- Spring Security: For secure authentication and authorization.
- Hibernate and Spring Data JPA: For ORM and database interaction.
- H2 Database: In-memory database for development and testing purposes.
- Swagger: For API documentation and testing.
- Maven: For dependency management and project build.

## Getting Started
### Prerequisites

- Java 11 or higher
- Maven

## Running the Application

1) Clone the repository:
   - git clone https://github.com/your-username/travel-agencies-management.git
   

2) Navigate to the project directory:
   - cd travel-agencies
   

3) Run the application using Maven:
   - mvn spring-boot:run

## API Documentation

After starting the application, access the Swagger UI to view and test the API endpoints at http://localhost:8080/swagger-ui/index.html.

## Security

The application uses Spring Security for authentication and authorization. Passwords are stored in an encrypted format using BCrypt. Role-based access control is implemented to differentiate between administrator and client functionalities.

## Contact
email@example.com

Project Link: https://gitlab.com/Mateusz299/travel-agencies

